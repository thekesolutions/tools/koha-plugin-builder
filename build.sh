#!/bin/sh

ARG=$1

if [ -z "$ARG" ]; then
    ARG=build
fi

cd /plugin
npm i
gulp $ARG
