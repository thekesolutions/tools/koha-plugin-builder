# koha-plugin-builder

## Introduction

This project aims to provide a docker container for building Koha plugins.

It is built on Node.js v12 (LTS).

## Usage

```shell
docker run \
    -v ${PWD}:/plugin \
    registry.gitlab.com/thekesolutions/tools/koha-plugin-builder
```

By default, it calls the `build` task, but it can be passed any task name like this:

```shell
docker run \
    -v ${PWD}:/plugin \
    registry.gitlab.com/thekesolutions/tools/koha-plugin-builder \
    static
```

## Old Node v8 plugins

In case you are building a plugin that hasn't been updated to Node.js v12 (LTS), you
need to use the *v1.x* images. The latest (an probably the last) is **v1.4**:

```shell
docker run \
    -v ${PWD}:/plugin \
    registry.gitlab.com/thekesolutions/tools/koha-plugin-builder:v1.4
```
