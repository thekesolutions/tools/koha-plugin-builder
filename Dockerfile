FROM node:12-alpine

RUN npm install -g gulp-cli

RUN apk --no-cache add zip jq git

COPY build.sh /build.sh

RUN mkdir /plugin

WORKDIR /plugin

ENTRYPOINT [ "/build.sh" ]
